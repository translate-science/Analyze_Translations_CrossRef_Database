# Analyze_Translations_CrossRef_Database

The code used to analyze the information in the CrossRef Database on works that are or have translations.

For more background information and the results of the analysis do have a look at our Wiki:
https://wiki.translatescience.org/wiki/Translated_articles_in_the_CrossRef_database